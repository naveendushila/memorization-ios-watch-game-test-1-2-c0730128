//
//  imageData.swift
//  Memorization WatchKit Extension
//
//  Created by MacStudent on 2019-03-14.
//  Copyright © 2019 Naveen Dushila. All rights reserved.
//

import Foundation
import UIKit
import WatchKit

class imageData: NSObject {
    
    static var gameLevel = -1
    
    static var images: [UIImage] = [UIImage(named: "0.jpg")!, UIImage(named: "1.jpg")!, UIImage(named:"2.jpg")!, UIImage(named:"3.jpg")!, UIImage(named:"4.jpg")!, UIImage(named:"5.jpg")!, UIImage(named:"6.jpg")!, UIImage(named:"7.jpg")!]

    static var imagesSelected: [Int] =  []
    static var questionNumber = 0
    
    static var answerSelected: [Int] = []
}
