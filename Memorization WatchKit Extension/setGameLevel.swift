//
//  setGameLevel.swift
//  Memorization WatchKit Extension
//
//  Created by MacStudent on 2019-03-14.
//  Copyright © 2019 Naveen Dushila. All rights reserved.
//

import WatchKit
import Foundation


class setGameLevel: WKInterfaceController {

    
    @IBAction func easyBtn() {
        imageData.gameLevel = 0
        pushController(withName: "gamePlay", context: nil)
    }
    
    @IBAction func hardBtn() {
        imageData.gameLevel = 1
        pushController(withName: "gamePlay", context: nil)
    }
    
    func changeIntent() {
        
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
