//
//  userInput.swift
//  Memorization WatchKit Extension
//
//  Created by MacStudent on 2019-03-14.
//  Copyright © 2019 Naveen Dushila. All rights reserved.
//

import WatchKit

class userInput: WKInterfaceController{
    
    let defaults = UserDefaults.standard
    var username = ""
    
    @IBOutlet weak var usernameBtn: WKInterfaceButton!
    @IBOutlet weak var playBtn: WKInterfaceButton!
    @IBOutlet weak var userInput: WKInterfaceLabel!
    @IBAction func enterUserName() {
        // 1. When person clicks on button, show them the input UI
        let suggestedResponses = ["In class", "Doing Assignment", "At movies", "Sleeping"]
        presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) { (results) in
            
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                self.defaults.set(userResponse, forKey: "userName")
                self.userInput.setText(userResponse)
                self.playBtn.setHidden(false)
            }
        }

    }
    
    //Tets
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
         playBtn.setHidden(true)
        
        username = defaults.string(forKey: "userName") ?? "nil"
        
        if username != "nil" {
            userInput.setText("Hello! " + username)
            usernameBtn.setHidden(true)
            playBtn.setHidden(false)
        }
        else {
            playBtn.setHidden(true)
            
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
}
