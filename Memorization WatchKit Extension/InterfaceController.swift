//
//  InterfaceController.swift
//  Memorization WatchKit Extension
//
//  Created by MacStudent on 2019-03-14.
//  Copyright © 2019 Naveen Dushila. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    var optionChoosed = -1
    var rightAnswer = -1
    var rightAnswerPosition = -1
    var answers = 0
    
    @IBOutlet weak var image0: WKInterfaceImage!
    @IBOutlet weak var image1: WKInterfaceImage!
    @IBOutlet weak var image2: WKInterfaceImage!
    @IBOutlet weak var image3: WKInterfaceImage!
    
    @IBOutlet weak var image4: WKInterfaceImage!
    @IBOutlet weak var image5: WKInterfaceImage!
    @IBOutlet weak var image6: WKInterfaceImage!
    @IBOutlet weak var image7: WKInterfaceImage!
    
    
    @IBOutlet weak var option0: WKInterfaceImage!
    @IBOutlet weak var option1: WKInterfaceImage!
    @IBOutlet weak var option2: WKInterfaceImage!
    @IBOutlet weak var option3: WKInterfaceImage!
    
    @IBOutlet weak var option4: WKInterfaceImage!
    @IBOutlet weak var option5: WKInterfaceImage!
    @IBOutlet weak var option6: WKInterfaceImage!
    @IBOutlet weak var option7: WKInterfaceImage!
    
    
    @IBOutlet weak var questionLabel: WKInterfaceLabel!
    
    @IBOutlet weak var btn0: WKInterfaceButton!
    @IBOutlet weak var btn1: WKInterfaceButton!
    @IBOutlet weak var btn2: WKInterfaceButton!
    @IBOutlet weak var btn3: WKInterfaceButton!
    
    @IBOutlet weak var btn4: WKInterfaceButton!
    @IBOutlet weak var btn5: WKInterfaceButton!
    @IBOutlet weak var btn6: WKInterfaceButton!
    @IBOutlet weak var btn7: WKInterfaceButton!
    
    @IBOutlet weak var optionGrp: WKInterfaceGroup!
    
    @IBOutlet weak var optionGrp2: WKInterfaceGroup!
    @IBOutlet weak var winLooseLbl: WKInterfaceLabel!
    
    @IBOutlet weak var playAgainBtn: WKInterfaceButton!
    
    @IBOutlet weak var grpOpt2: WKInterfaceGroup!
    
    @IBOutlet weak var grpOptNum2: WKInterfaceGroup!
    
    
    
    @IBAction func playAgain() {
        
        print("Let's play again!")
        
        self.willActivate()
        self.awake(withContext: self)
        optionGrp.setHidden(false)
    }
    
    
    
    @IBAction func choose0() {
        optionChoosed = 0
        checkAnswer()
    }
    
    @IBAction func choose1() {
        optionChoosed = 1
           checkAnswer()
    }
    
    @IBAction func choose2() {
        optionChoosed = 2
           checkAnswer()
    }
    
    @IBAction func choose3() {
        optionChoosed = 3
           checkAnswer()
    }
    
    
    @IBAction func choose4() {
        optionChoosed = 2
        checkAnswer()
    }
    
    @IBAction func choose5() {
        optionChoosed = 2
        checkAnswer()
    }
    
    @IBAction func choose6() {
        optionChoosed = 2
        checkAnswer()
    }
    
    @IBAction func choose7() {
        optionChoosed = 2
        checkAnswer()
    }
    
    //Tets
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        print ("Game Level \(imageData.gameLevel)")
        
        if (imageData.gameLevel == 0) {
            
            grpOpt2.setHidden(true)
            grpOptNum2.setHidden(true)
            optionGrp2.setHidden(true)
            
        }
        
        winLooseLbl.setHidden(true)
        playAgainBtn.setHidden(true)
        
        chooseRandomImages()
    
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    func chooseRandomImages () {
        
        imageData.imagesSelected = [Int.random(in: 0 ... 7),Int.random(in: 0 ... 7),Int.random(in: 0 ... 7),Int.random(in: 0 ... 7),Int.random(in: 0 ... 7),Int.random(in: 0 ... 7),Int.random(in: 0 ... 7),Int.random(in: 0 ... 7)]
       
        
        image0.setImage(imageData.images[imageData.imagesSelected[0]])
        image1.setImage(imageData.images[imageData.imagesSelected[1]])
        image2.setImage(imageData.images[imageData.imagesSelected[2]])
        image3.setImage(imageData.images[imageData.imagesSelected[3]])
        
        image4.setImage(imageData.images[imageData.imagesSelected[4]])
        image5.setImage(imageData.images[imageData.imagesSelected[5]])
        image6.setImage(imageData.images[imageData.imagesSelected[6]])
        image7.setImage(imageData.images[imageData.imagesSelected[7]])
        
        for i in 0..<imageData.imagesSelected.count {
            print(imageData.imagesSelected[i])
        }
        
        // Thread.sleep(until: Date(timeIntervalSinceNow: 5.0))
        let timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { (timer) in
            
            //Running code after 3 sec
            //Hiding images and showing options
            self.hideimages()
            self.chooseAnswers()
        }
        

    }
    
    func  hideimages() {
        image0.setImage(UIImage(named: "lock.jpg"))
        image1.setImage(UIImage(named: "lock.jpg"))
        image2.setImage(UIImage(named: "lock.jpg"))
        image3.setImage(UIImage(named: "lock.jpg"))
        image4.setImage(UIImage(named: "lock.jpg"))
        image5.setImage(UIImage(named: "lock.jpg"))
        image6.setImage(UIImage(named: "lock.jpg"))
        image7.setImage(UIImage(named: "lock.jpg"))
    }
    
    func chooseAnswers() {
        
        if (imageData.gameLevel == 0) {
        imageData.questionNumber = Int.random(in: 1...4)
        }
        else {
            imageData.questionNumber = Int.random(in: 1...8)
        }
        
        print ("Question Number: \(imageData.questionNumber)")
        questionLabel.setText(String(imageData.questionNumber))
        
        imageData.questionNumber = imageData.questionNumber - 1
        
        rightAnswer = imageData.imagesSelected[imageData.questionNumber]
        rightAnswerPosition = Int.random(in: 0...3)

        print("Right Answer: \(rightAnswer)")
        print("Right Answer Position: \(rightAnswerPosition)")
        
        imageData.answerSelected = [Int.random(in: 0...7),Int.random(in: 0...7),Int.random(in: 0...7),Int.random(in: 0...7)]
        
        if (rightAnswerPosition == 0) {
            option0.setImage(imageData.images[rightAnswer])
            option1.setImage(imageData.images.randomElement())
            option2.setImage(imageData.images.randomElement())
            option3.setImage(imageData.images.randomElement())
             option4.setImage(imageData.images.randomElement())
             option5.setImage(imageData.images.randomElement())
             option6.setImage(imageData.images.randomElement())
             option7.setImage(imageData.images.randomElement())
        
        }
        else if (rightAnswerPosition == 1) {
            option1.setImage(imageData.images[rightAnswer])
            option0.setImage(imageData.images.randomElement())
            option2.setImage(imageData.images.randomElement())
            option3.setImage(imageData.images.randomElement())
            option4.setImage(imageData.images.randomElement())
            option5.setImage(imageData.images.randomElement())
            option6.setImage(imageData.images.randomElement())
            option7.setImage(imageData.images.randomElement())
        }
        else if (rightAnswerPosition == 2) {
            option2.setImage(imageData.images[rightAnswer])
            option1.setImage(imageData.images.randomElement())
            option0.setImage(imageData.images.randomElement())
            option3.setImage(imageData.images.randomElement())
            option4.setImage(imageData.images.randomElement())
            option5.setImage(imageData.images.randomElement())
            option6.setImage(imageData.images.randomElement())
            option7.setImage(imageData.images.randomElement())
        }
        else if (rightAnswerPosition == 3) {
            option3.setImage(imageData.images[rightAnswer])
            option1.setImage(imageData.images.randomElement())
            option2.setImage(imageData.images.randomElement())
            option0.setImage(imageData.images.randomElement())
            option4.setImage(imageData.images.randomElement())
            option5.setImage(imageData.images.randomElement())
            option6.setImage(imageData.images.randomElement())
            option7.setImage(imageData.images.randomElement())
        }
        else if (rightAnswerPosition == 4) {
            option4.setImage(imageData.images[rightAnswer])
            option1.setImage(imageData.images.randomElement())
            option2.setImage(imageData.images.randomElement())
            option0.setImage(imageData.images.randomElement())
            option3.setImage(imageData.images.randomElement())
            option5.setImage(imageData.images.randomElement())
            option6.setImage(imageData.images.randomElement())
            option7.setImage(imageData.images.randomElement())
        }
        else if (rightAnswerPosition == 5) {
            option5.setImage(imageData.images[rightAnswer])
            option1.setImage(imageData.images.randomElement())
            option2.setImage(imageData.images.randomElement())
            option0.setImage(imageData.images.randomElement())
            option4.setImage(imageData.images.randomElement())
            option3.setImage(imageData.images.randomElement())
            option6.setImage(imageData.images.randomElement())
            option7.setImage(imageData.images.randomElement())
        }
        else if (rightAnswerPosition == 6) {
            option6.setImage(imageData.images[rightAnswer])
            option1.setImage(imageData.images.randomElement())
            option2.setImage(imageData.images.randomElement())
            option0.setImage(imageData.images.randomElement())
            option4.setImage(imageData.images.randomElement())
            option5.setImage(imageData.images.randomElement())
            option3.setImage(imageData.images.randomElement())
            option7.setImage(imageData.images.randomElement())
        }
        else if (rightAnswerPosition == 7) {
            option6.setImage(imageData.images[rightAnswer])
            option1.setImage(imageData.images.randomElement())
            option2.setImage(imageData.images.randomElement())
            option0.setImage(imageData.images.randomElement())
            option4.setImage(imageData.images.randomElement())
            option5.setImage(imageData.images.randomElement())
            option3.setImage(imageData.images.randomElement())
            option7.setImage(imageData.images.randomElement())
        }
    }
 
    func checkAnswer() {
        
        print("imageData.imagesSelected[optionChoosed]: \(imageData.imagesSelected[optionChoosed])")
        print("imageData.imagesSelected[rightAnswerPosition]: \(imageData.imagesSelected[rightAnswerPosition])")
         // print("imageData.imagesSelected[rightAnswer]: \(imageData.imagesSelected[rightAnswer])")
        var Decision = ""
        
        if (optionChoosed == rightAnswerPosition) {
            print("You Win")
            Decision = "You Win!"
        }
        else
        {
            print("You Loose!")
            Decision = "You Loose!"
        }
        optionGrp.setHidden(true)
        optionGrp2.setHidden(true)
        winLooseLbl.setText(Decision)
        
        winLooseLbl.setHidden(false)
        playAgainBtn.setHidden(false)
        
    }

    
    
}
